import axios from 'axios'

const state = {
    albums: [],
    album: {},
}

const getters = {
    getAllAlbum: state => state.albums,
    getAlbum: state => state.albums
}

const mutations = {
    setAllAlbum(state, albums) {
        state.albums = albums
    },
    setAlbum(state, album) {
        state.album = album
    }
}

const actions = {
    async fetchAllAlbum({ commit }) {
        const { data } = await axios.get('http://localhost:8888/photos_api/api/albums')
        commit('setAllAlbum', data.albums)
    },
    async fetchAlbum({ commit }, id) {
        const { data } = await axios.get('http://localhost:8888/photos_api/api/album/' + id)
        commit('setAlbum', data.album)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}

import axios from 'axios'

const state = {
    pictures: [],
    picture: {},
}

const getters = {
    getAllPicture: state => state.pictures,
    getPicture: state => state.pictures
}

const mutations = {
    setAllPicture(state, pictures) {
        state.pictures = pictures
    },
    setPicture(state, picture) {
        state.picture = picture
    }
}

const actions = {
    async fetchAllPicture({ commit }) {
        const { data } = await axios.get('http://localhost:8888/photos_api/api/pictures')
        commit('setAllPicture', data.pictures)
    },
    async fetchPicture({ commit }, id) {
        const { data } = await axios.get('http://localhost:8888/photos_api/api/picture/' + id)
        commit('setPicture', data.picture)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}

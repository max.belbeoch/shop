import axios from '../../api/config';

const state = {
    orders: [],
    order: null,
}

const getters = {
    getAllOrder: state => state.order,
    getOrder: state => state.orders
}

const mutations = {
    setAllOrder(state, orders) {
        state.orders = orders
    },
    setOrder(state, order) {
        state.order = order
    }
}

const actions = {
    async fetchAllOrder({ commit }) {
        await axios.get("http://localhost:8888/photos_api/api/orders")
            .then((res) => {
                console.log(res)
                if (res.data.result) {
                    commit('setAllOrder', res.data.orders)
                    //this.$router.push({ name: "profile" });
                } else {
                    this.error = res.data.error;
                }
            })
            .catch( () => {
                this.error = "Une erreur est survenue";
            });
    },
    async fetchOrder({ commit }) { // to update after testing url with id
        const { data } = await axios.get('http://localhost:8888/photos_api/api/order/id')
        commit('setOrder', data.result)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}

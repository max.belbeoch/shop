import { createStore } from 'vuex';
import picture from '@/store/modules/picture';
import album from '@/store/modules/album';
import order from '@/store/modules/order';

export default createStore({
  modules: {
    picture,
    album,
    order
  }
});
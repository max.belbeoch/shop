
import {
  createRouter,
  createWebHashHistory
} from 'vue-router';

import userApi from '@/api/user';
import Home from '@/components/Home.vue';
import Register from '@/components/Register';
import Login from '@/components/Login';
import Profile from '@/components/Profile.vue';
import Picture from '@/components/Picture.vue';
import Albums from '@/components/Albums.vue';
import Cart from '@/components/Cart.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/albums',
    name: 'albums',
    component: Albums
  },
  {
    path: '/photos',
    name: 'pictures',
    component: Home
  },
  {
    path: '/photo/:id',
    name: 'picture',
    component: Picture
  },
  {
    path: '/commandes',
    name: 'profile',
    component: Profile,
    meta: {
      require_auth: true,
    }
  },
  {
    path: '/panier',
    name: 'cart',
    component: Cart,
    meta: {
      require_auth: true,
    }
  },
  {
    component: Login,
    path: '/connexion',
    name: 'login'
  },
  {
    component: Register,
    path: '/inscription',
    name: 'register'
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  base: process.env.BASE_URL,
  routes
});

// check token if route has require_auth meta = true
router.beforeEach(async (to, from, next) => {
  const isLogged = await userApi.verifyUser();
  if (!isLogged.data.result) {
    localStorage.removeItem('token');
    localStorage.setItem("is_logged", false);
    if (to.matched.some(route => route.meta.require_auth)) {
      return next('/connexion');
    }
    return next();
  } else {
    localStorage.setItem("is_logged", true);
    return next();
  }
})

export default router

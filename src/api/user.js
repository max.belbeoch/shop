import axios from './config'

export default {
  verifyUser: () => {
    return axios
      .get(
        "http://localhost:8888/photos_api/api/is_logged",
        {
          headers: {
            Authorization: localStorage.getItem('token'),
          },
        }
      )
  }
}
